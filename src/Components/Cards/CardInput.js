import React from "react";

export const InputCard = ({close, change, click, val}) => {
    return (
        <div className="card-holder">
            <div className="card">
                <input type="text" onSubmit={(e) => e.preventDefault()}  value={val} onChange={change} placeholder="Add a new card here ..." />
            </div>
            <div className="card-content">
                <input type="submit" style = {{"cursor":"pointer"}} onClick={click} value="Add another card" className="btn" />
                <p style = {{"cursor":"pointer"}} className="close-card" onClick={() => close()}>X</p>
            </div>
        </div>
    )
}