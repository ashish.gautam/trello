import React, { Component } from "react";
import { InputCard } from "./CardInput";

export default class CardElement extends Component {
    constructor(props) {
        super(props)

        this.state = {
            showCard: false,
            cardInput: '',

        }
    }

    addCard = (e) => {
        e.preventDefault()
        const cardName = this.state.cardInput;
        if (cardName) {
            this.props.getName(cardName, this.props.id)
            this.setState({
                cardInput: ''
            })
        }
        this.setState({
            cardInput: ''
        })
    }

    handleClose = (e) => {
        this.setState({
            showCard: false
        })
    }
    getCardInput = (e) => {
        this.setState({
            cardInput: e.target.value
        })
    }

    handleClick = (e) => {
        this.setState({
            showCard: true,

        })

    }

    render() {
        return (
            <div className="card-div">
                {
                    this.state.showCard
                        ? <InputCard change={this.getCardInput} val={this.state.cardInput} click={this.addCard} close={this.handleClose} />
                        : <button className="pink-btn" onClick={this.handleClick} >
                            ADD ANOTHER LIST
                    </button>
                }
            </div>
        )
    }
}