export const getData = function(){
    return fetch(`https://api.trello.com/1/boards/5b235d1008f4396552a52c42?key=6b3004cf200d26aba3c1e1982a6fa86a&token=b18c67749bedf3b0e4eef0b08fa3223217750c208ead7c8ffa96b7ec2fc8c4f2`);      
}

export const getTrelloList = () => {
    return fetch(`https://api.trello.com/1/boards/ua2BEzh7/lists?cards=open&card_fields=name&filter=open&fields=all&key=6b3004cf200d26aba3c1e1982a6fa86a&token=b18c67749bedf3b0e4eef0b08fa3223217750c208ead7c8ffa96b7ec2fc8c4f2`);
}

export const getCards = (id) => {
    return fetch(`https://api.trello.com/1/lists/${id}/cards?key=6b3004cf200d26aba3c1e1982a6fa86a&token=b18c67749bedf3b0e4eef0b08fa3223217750c208ead7c8ffa96b7ec2fc8c4f2`);
}

export const createTrelloList = (name) => {
    return fetch(`https://api.trello.com/1/boards/ua2BEzh7/lists?name=${name}&pos=bottom&key=6b3004cf200d26aba3c1e1982a6fa86a&token=b18c67749bedf3b0e4eef0b08fa3223217750c208ead7c8ffa96b7ec2fc8c4f2`, {
        method: 'POST'
    }
    )
}
export const createCard = (name, id) => {
    return fetch(`https://api.trello.com/1/cards?name=${name}&idList=${id}&keepFromSource=all&key=6b3004cf200d26aba3c1e1982a6fa86a&token=b18c67749bedf3b0e4eef0b08fa3223217750c208ead7c8ffa96b7ec2fc8c4f2`,
    { method: 'POST' }
    
    )
}