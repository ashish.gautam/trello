import React from "react";
// use props and then take required value or function from it
// why don't u simply use a button??
export default (props) => (
    <div>
        <button className="pink-btn" onClick={() => props.clickEvent()} >
            ADD ANOTHER LIST
        </button>
    </div>
)