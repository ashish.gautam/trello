import React from 'react';
import { getCards, createCard } from '../Data/getDataFromTrello';
import Card from '../Cards/Cards';
import CardElement from "../Cards/CardElement";


class ListElement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cardData: [],
            loading: true,
        }
    }

    getCardName = (name, id) => {
        console.log(name, id, " entered get card")
        if (name && id) {
            createCard(name, id)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        cardData: [...this.state.cardData, data]
                    })
                }, () => console.log(this))
                .catch(console.log)
        } else {
            return
        }
    }
    componentDidMount() {
        console.log("list type", this.props.listData);
        getCards(this.props.listData.id)
            .then(response => response.json())
            .then(data => {
                console.log("list element", data);
                this.setState({
                    cardData: data,
                    loading: false,
                })
            })
            .catch(error => {
                this.setState({ loading: false });
                console.log("error", error);
            })
    }
    // use let instead of var
    render() {
        if (this.state.loading) {
            return <p>Loading</p>
        } else {
            return (<div className="list-div" key={this.props.listData.id}  >
                <div className="head-name">{this.props.listData.name}</div>
                <Card data={this.state.cardData} />
                <CardElement id={this.props.listData.id} getName={this.getCardName} />
            </div>)
        }

    }
}

export default ListElement;