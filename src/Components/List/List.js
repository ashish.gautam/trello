import React from 'react';
import { getTrelloList, createTrelloList } from '../Data/getDataFromTrello';
import ListElement from './ListElement';
import ListInput from './ListInput';
import ListAddBtn from './ListAddBtn'

class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.id,
            showInput: false,
            inputName: "",
            ListData: [],
            loading: true,
        }
        // no need of binding in ES6
        // use camel-case for function names and all-caps for constants
        // this.AddList = this.AddList.bind(this);
    }

    addList(event) {
        console.log(event.target.parentNode.previousSibling);
    }
    //why don't u simply use a toggle function to handle close and open?
    handleClose = () => {
        this.setState({
            showInput: false
        })
    }

    handleClick = () => {
        this.setState({
            showInput: true
        })
    }
    getName = (e) => {
        this.setState({
            inputName: e.target.value.trim()
        })
    }

    getInputValue = (e) => {
        const listName = this.state.inputName;
        if (listName) {
            createTrelloList(listName)
                .then(response => response.json())
                .then(data => {
                    console.log(data, " before")
                    data["cards"] = [];
                    this.setState({
                        ListData: [...this.state.ListData, data],
                        inputName: '',

                    })
                }, () => { console.log(this.state.ListData, " after") })
                .catch((error) => {
                    console.log(error);
                })
        } else {
            return
        }
    }
    // why empty console.log statement?
    // why no catch statement to handle error?
    componentDidMount() {
        getTrelloList().then(resp => resp.json())
            .then(data => {
                this.setState({
                    ListData: data,
                    loading: false,
                }, () => {
                    console.log("get trello list ", data);
                })
            }).catch(err => {
                this.setState({ loading: false });
                console.error("error occured", err);
            });
    }
    // avoid inline styling
    // classnames starts with small letter
    render() {
        return (<div className="List" style={{ "display": "flex", "justifyContent": "space-around", padding: 20 }}>
            {
                (!this.state.loading)
                    ? (
                        this.state.ListData.map((listType) => {
                            return <ListElement key={listType.id} listData={listType} />
                        })
                    )
                    : <p className="loader">Loading existing cards...</p >
            }
            {this.state.showInput
                ? <ListInput clickEvent={this.handleClose} getName={this.getName} val={this.state.inputName} getInputValue={this.getInputValue} />
                : <ListAddBtn clickEvent={this.handleClick} />
            }
        </div>
        )
    }
}

export default List;