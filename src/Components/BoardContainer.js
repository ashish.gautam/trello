import React from 'react';
import { getData } from '../Components/Data/getDataFromTrello';

class BoardContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            boardData: '',
        }
    }
    componentDidMount() {
        getData().then(resp => resp.json())
            .then(data => {
                this.setState({
                    boardData: data
                })
            })
    }
    render() {
        const { name } = this.state.boardData;
        return (
            <div style={{ backgroundColor: 'pink', color: 'white' }}>
                <h1 style={{ margin: 0, padding: 10 }}>{name}<span role="img" aria-labelledby="lol">😂</span></h1>
            </div>
        );
    }
}

export default BoardContainer;