import React, { Component } from 'react';
import './App.css';
import Board from './Components/BoardContainer'
import List from './Components/List/List'

class App extends Component {
  render() {
    return (
      <div className="main-container">
      <div className="App overflow-auto">
        <Board />
        <List />
      </div>
      </div>
    );
  }
}

export default App;
